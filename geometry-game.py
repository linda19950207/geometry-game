import math
import random
import turtle


# Create a point
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def in_rectangle(self, rectangle):
        if (
            rectangle.point1.x < self.x
            and self.x < rectangle.point2.x
            and rectangle.point1.y < self.y
            and self.y < rectangle.point2.y
        ):
            return True
        else:
            return False

    def distance_of_two_points(self, point):
        distance = math.sqrt((self.x - point.x) ** 2 + (self.y - point.y) ** 2)
        return distance


# Create a rectangle
class Rectangle:
    def __init__(self, point1, point2):
        self.point1 = point1
        self.point2 = point2

    def area(self):
        area = (self.point2.x - self.point1.x) * (self.point2.y - self.point1.y)
        return area


class DrawRectangle(Rectangle):
    def draw(self, canvas):
        # Go to a certain coordinate
        canvas.penup()
        canvas.goto(self.point1.x, self.point1.y)

        # Move to form the rectangle
        canvas.pendown()
        canvas.forward(self.point2.x - self.point1.x)
        canvas.left(90)
        canvas.forward(self.point2.y - self.point1.y)
        canvas.left(90)
        canvas.forward(self.point2.x - self.point1.x)
        canvas.left(90)
        canvas.forward(self.point2.y - self.point1.y)


class DrawPoint(Point):
    def draw(self, canvas, size=5, color="red"):
        canvas.penup()
        canvas.goto(self.x, self.y)
        canvas.pendown()
        canvas.dot(size, color)


draw_the_rectangle = DrawRectangle(
    Point(random.randint(0, 9), random.randint(0, 9)),
    Point(random.randint(10, 19), random.randint(10, 19)),
)
print(draw_the_rectangle.area())

# Create rectangle object
rectangle = DrawRectangle(
    Point(random.randint(0, 9), random.randint(0, 9)),
    Point(random.randint(10, 19), random.randint(10, 19)),
)

# Print rectangle coordinates
print(
    "Rectangle Coordinates: ",
    rectangle.point1.x,
    ",",
    rectangle.point1.y,
    "and",
    rectangle.point2.x,
    ",",
    rectangle.point2.y,
)

# Get area from user and compare it to the correct area
user_input_area = float(input("Guess the area: "))
if user_input_area == rectangle.area():
    print(True)
else:
    print("Your area is off by: ", abs(user_input_area - rectangle.area()))

# Get point from user and print out the result
user_input_point = DrawPoint(float(input("Guess x: ")), float(input("Guess y: ")))
print("You point is in side the rectangle: ", user_input_point.in_rectangle(rectangle))

# Draw the recatngle and the user input point
myturtle = turtle.Turtle()
canvas = turtle.Screen()
rectangle.draw(canvas=myturtle)
user_input_point.draw(canvas=myturtle)

turtle.done()
